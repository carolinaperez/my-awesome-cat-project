Después de instalar Git en el ordenador y ejecutarlo modificamos el usuario en la configuración.
ls
para saber donde estamos
cd D:
para ir al disco D
mkdir repositorio
para crear una nueva carpeta en D
touch index.html
para crear un archivo html en la carpeta que he creado
git clone https://gitlab.com/carolinaperez/my-awesome-cat-project.git
para crear un repositorio en el proyecto de gitlab
cd my-awesome-cat-project
selecciono esta carpeta
touch README.md
creo un archivo readme.md
una vez editado el archivo con lo que queremos podemos hacer un add para subirlo al área staged
una vez subido podemos hacer un commit con un comentario
confirmamos que está todo bien con push origin main
